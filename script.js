document.addEventListener('DOMContentLoaded', function() {
  const titles = document.querySelectorAll('.title');
  const cards = document.querySelectorAll('.card');
  const addLists = document.querySelectorAll('.add-list');
  const addCards = document.querySelectorAll('.add-card');

  titles.forEach(title => {
      title.addEventListener('click', () => {
          title.contentEditable = true;
          title.classList.add('editing');
          title.focus();
      });

      title.addEventListener('blur', () => {
          title.contentEditable = false;
          title.classList.remove('editing');
      });
  });

  cards.forEach(card => {
      card.addEventListener('click', () => {
          card.contentEditable = true;
          card.classList.add('editing');
          card.focus();
      });

      card.addEventListener('blur', () => {
          card.contentEditable = false;
          card.classList.remove('editing');
      });
  });

  addLists.forEach(addList => {
      addList.addEventListener('click', () => {
          const newList = document.createElement('div');
          newList.classList.add('list');
          newList.innerHTML = `
              <div class="title removable editable">New List</div>
              <div class="content"></div>
              <div class="add-card editable">Add another card</div>
          `;
          document.getElementById('main').insertBefore(newList, addList);
      });
  });

  addCards.forEach(addCard => {
      addCard.addEventListener('click', () => {
          const newCard = document.createElement('div');
          newCard.classList.add('card', 'removable', 'editable');
          newCard.innerText = 'New Card';
          addCard.previousElementSibling.appendChild(newCard);
      });
  });
});
